import datetime

from django.db.models import Q
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from .models import Profile


# Create your views here
def facebook(request):
    if request.method == 'POST':
        username = request.POST.get('first_name')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        password = request.POST.get('password')
        dob = request.POST.get('date')
        gender = request.POST.get('gender')
        user = User.objects.create(username=username,  email=email, password=password)
        Profile.objects.create(user=user, first_name=first_name, dob=dob, last_name=last_name, email=email, gender=gender)
    return render(request, 'polls/facebook.html')


def google(request):
    return render(request, 'polls/google.html')


def singup(request):
    if request.method == 'POST':
            username = request.get('username')
            password = request.get('password1')
            user = authenticate(username=username, password=password)
            print(user, 'this is user')
            if user is not None:
                login(request, user)
                return redirect('google/')
            else:
                print("invalidd")

    return render(request, 'polls/facebook.html')


def dashboard(request):
    profiles = Profile.objects.all()
    profiles1 = Profile.objects.filter(first_name__startswith='a')
    profiles2 = Profile.objects.filter(gender='Male')
    profiles3 = Profile.objects.filter(gender='Female')
    profiles4 = Profile.objects.all().order_by('-date_joined')

    print(profiles)
    print(profiles1)
    print(profiles2)
    print(profiles3)
    print(profiles4)

    return render(request, 'polls/dashboard.html', {'profiles': profiles, 'profiles1': profiles1, 'profiles2': profiles2 , 'profiles3': profiles3, 'profiles34': profiles4})









#practice CRUD app

from django.shortcuts import render, redirect
from .models import Provinces



def index(request):
    provinces = Provinces.objects.all()
    context = {'provinces': provinces}
    return render(request, 'polls/index.html', context)


def create(request):
    print(request.POST)
    province = Provinces(province_name=request.POST['province_name'], minister_name=request.POST['minister_name'],
                         capital=request.POST['capital'], districts=request.POST['districts'])
    province.save()
    return redirect('home/')


def destroy(request, id):
    province = Provinces.objects.get(id=id)
    province.delete()
    return render(request, 'polls/index.html')


def update(request):
    provinces = Provinces.objects.all()
    context = {'provinces': provinces}
    return render(request, 'polls/edit.html', context)