from django.contrib.auth.decorators import login_required
from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('facebook/', views.facebook, name='facebook'),
    path('google/', views.google, name='google'),
    path('dashboard/', views.dashboard, name='dashboard'),



   #CRUP app url
    path('home/', views.index),
    url(r'^create', views.create),
    url(r'^delete/(?P<id>\d+)$', views.destroy),
    path('update', views.update),

]